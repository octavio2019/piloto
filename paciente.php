<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript">
  $(function() {
              $("#nombre").autocomplete({
                  source: "consulta.php",
                  minLength: 2,
                  select: function(event, ui) {
  					event.preventDefault();
                      $('#nombre').val(ui.item.nombre);
  					$('#edad').val(ui.item.edad);
  					$('#fecha').val(ui.item.fecha);
  					$('#id_paciente').val(ui.item.$id_paciente);
  			     }
              });
  		});
</script>
 <title>Alta de Pacientes</title>
 </head>
<body>

<?php
error_reporting(0);
include("query/conexion.php");
include ("plantilla/header.php");
include("query/insertar.php");

 ?>
<div class="container">
  <div id="titulo">

    <h3>Alta de Pacientes</h3>

    </div>
</div>
<br><br><br><br>

<div class= class="ui-widget">
<div class="row">

   <form action="#" method="POST" autocomplete="off">
     <p>Paciente</p>
          <input placeholder="Ingrese Nombre Completo" id="nombre" name="nombre" type="text" class="validate" required>
          <p>Edad</p>
          <input type="text" name="edad" placeholder="Ingrese su edad" id="edad" class="validate" required >
          <p>Fecha de Nacimiento</p>
          <input type="date" name="fecha" id="fecha" step="1" class="validate">


    <br>
    <p>*Actualizacion y Nuevos Registros</p>
        <input type="submit" name="boton" id="registrar" value="insertar" class="btn btn-success" />
        <input type="submit" name="boton" id="registrar" value="actualizar" class="btn btn-success" />
        <input type="submit" name="boton" id="registrar" value="borrar" class="btn btn-success" />
</form>
</div>
</div>

</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#fecha").datepicker({
			changeMonth: true,
      		changeYear: true,
      		yearRange: '1900:' + 3000,
			dateFormat: "yy-mm-dd"
		});

		$('#fecha').change(function(){
			$.ajax({
				type:"POST",
				data:"fecha=" + $('#fecha').val(),
				url:"query/calcula.php",
				success:function(r){
					$('#fecha').text(r);
				}
			});
		});
	});
</script>
